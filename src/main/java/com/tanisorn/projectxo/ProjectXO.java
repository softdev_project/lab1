/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.tanisorn.projectxo;

import java.util.Scanner;

/**
 *
 * @author uSeR
 */
public class ProjectXO {

    Scanner sc = new Scanner(System.in);
    public boolean play = false;
    public String start;
    private char[][] board;
    public String turn;

    public void GameStart() {

        System.out.println("+----------------------+");
        System.out.println("| Welcome to OX game!!!|");
        System.out.println("+----------------------+");
        System.out.print("Play To Game??? (y/n): ");
        start = sc.nextLine().toLowerCase();
        while (!start.equals("n") && !start.equals("y")) {
            System.out.print("Again Please!!! (y/n): ");
            start = sc.nextLine().toLowerCase();
        }
        if (start.equals("n")) {
            play = false;
        } else {
            play = true;
        }
    }

    private void printBoard() {
        System.out.println("_____________");
        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " | ");
            }
            System.out.println();
            System.out.println("_____________");
        }
    }

    public void newBoard() {
        board = new char[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '-';
            }
        }
    }

    private boolean Checkwin() {
        // Check rows
        for (int i = 0; i < 3; i++) {
            if (board[i][0] != '-' && board[i][0] == board[i][1] && board[i][0] == board[i][2]) {
                return true;
            }
        }

        // Check columns
        for (int i = 0; i < 3; i++) {
            if (board[0][i] != '-' && board[0][i] == board[1][i] && board[0][i] == board[2][i]) {
                return true;
            }
        }

        // Check diagonals
        if (board[0][0] != '-' && board[0][0] == board[1][1] && board[0][0] == board[2][2]) {
            return true;
        }

        if (board[0][2] != '-' && board[0][2] == board[1][1] && board[0][2] == board[2][0]) {
            return true;
        }
        return false;
    }

    public boolean isBoardFull() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    public void play() {
        Scanner sc = new Scanner(System.in);
        newBoard();
        printBoard();
        char currentPlayer = 'X';
        boolean isGameFinished = false;
        while (!isGameFinished) {
            System.out.print("It's player " + currentPlayer + " turn. Please input row : ");
            int row = sc.nextInt() - 1;
            System.out.print("It's player " + currentPlayer + " turn. Please input column : ");
            int col = sc.nextInt() - 1;

            if (row >= 0 && row < 3 && col >= 0 && col < 3 && board[row][col] == '-') {
                board[row][col] = currentPlayer;
                printBoard();

                if (Checkwin()) {
                    System.out.println("Congratulations! Player " + currentPlayer + " wins!");
                    System.out.println("________________________________");
                    isGameFinished = true;
                } else if (isBoardFull()) {
                    System.out.println("!!! Draw !!!");
                    isGameFinished = true;
                } else {
                    currentPlayer = (currentPlayer == 'X') ? 'O' : 'X';
                }
            } else {
                System.out.println(" Please Input Agian!!.");
            }
        }
        
        System.out.print("Do you want to play again? (y/n) : ");
        String newGame = sc.next();
        if (newGame.equals("y")) {
            play();

        } else {
            System.out.println("+--------------------+");
            System.out.println("----^^ Good Bye ^^----");
            System.out.println("+--------------------+");
            System.exit(0);
        }
    }

    public static void main(String[] args) {
        ProjectXO project = new ProjectXO();
        Scanner sc = new Scanner(System.in);
        project.GameStart();
        if (project.play == false) {
            System.out.println("ByeBye!!");
            System.exit(0);
        }else {
        project.play();
        }
    }
}
